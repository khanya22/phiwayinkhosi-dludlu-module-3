import 'package:myapp/Screens/checkout_screen.dart';
import 'package:myapp/Screens/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:myapp/Screens/login.dart';
import 'package:myapp/Screens/signup.dart';

void main() => runApp(const African_Black_Child());

class African_Black_Child extends StatelessWidget {
  const African_Black_Child({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
            primary: const Color.fromARGB(255, 247, 184, 68),
            secondary: const Color.fromARGB(255, 78, 107, 139),
            background: Colors.white30),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      title: 'African Black Child',
      debugShowCheckedModeBanner: false,
      home: Login(),

      //Routing table
      routes: {
        Dashboard.routeName: (ctx) => const Dashboard(),
        '/signup': (_) => const SignUp(),
        '/login': (_) => Login(),
        '/checkout': (_) => const Checkout(),
        '/searchbar': (_) => const Searchbar()
      },
    );
  }
}
