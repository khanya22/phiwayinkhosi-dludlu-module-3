import 'package:flutter/material.dart';

class Checkout extends StatelessWidget {
  const Checkout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const [
            Text(
              "Checkout",
              style: TextStyle(color: Colors.black),
            ),
            Icon(
              Icons.shopping_cart_rounded,
              color: Colors.black,
            ),
          ],
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.of(context).pushNamed('/dashboard');
          },
          color: Colors.black,
          tooltip: 'Back',
        ),
        actions: [],
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: ListView(children: <Widget>[
        Container(
          decoration: const BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Color.fromARGB(255, 122, 122, 122), width: 1)),
          ),
          child: Row(
            children: const <Widget>[
              Padding(
                padding: EdgeInsets.all(15),
                child: Text("Tshirt",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "Price R200.00",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ],
          ),
        ),
        Container(
            decoration: const BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: Color.fromARGB(255, 122, 122, 122), width: 1)),
            ),
            child: Row(children: const <Widget>[
              Padding(
                padding: EdgeInsets.all(15),
                child: Text("Sweater",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    )),
              ),
              Spacer(),
              Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Price R500.00",
                      style: TextStyle(fontWeight: FontWeight.bold)))
            ]))
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.shopping_cart_checkout_rounded),
      ),
    );
  }
}
