import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

void main() {
  runApp(const Dashboard());
}

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);
  static const routeName = '/dashboard';
  @override
  State<Dashboard> createState() => _FindResState();
}

int currentIndex = 0;
final screens = [
  const HomeScreen(),
  const Search_Screen(),
  const WishList(),
  const Account()
];

class _FindResState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "African Black Child",
          theme: ThemeData(
            primaryTextTheme: Typography.blackRedmond,
            iconTheme: IconThemeData(color: Colors.black),
            buttonTheme: ButtonThemeData(buttonColor: Colors.black),
            colorScheme: const ColorScheme(
                primary: Colors.white,
                background: Color.fromARGB(255, 255, 255, 255),
                onPrimary: Colors.black,
                brightness: Brightness.light,
                onSecondary: Color.fromARGB(255, 255, 254, 254),
                error: Colors.red,
                onError: Colors.red,
                secondary: Color.fromARGB(255, 0, 0, 0),
                onBackground: Color.fromARGB(255, 0, 0, 0),
                onSurface: Color.fromARGB(255, 255, 253, 253),
                surface: Colors.black),
          ),
          home: Scaffold(
            body: screens[currentIndex],

            //Navigation Bar
            bottomNavigationBar: BottomNavigationBar(
                showUnselectedLabels: true,
                currentIndex: currentIndex,
                onTap: (index) => setState(() => currentIndex = index),
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                      icon: Icon(Icons.home), label: "Home"),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.search), label: "Search"),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.favorite), label: "Wish List"),
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.person_outline_rounded,
                    ),
                    label: "Account",
                  )
                ]),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/checkout');
              },
              child: const Icon(Icons.shopping_cart_checkout_rounded),
            ),
          ));
    });
  }
}

//Account screen
class Account extends StatefulWidget {
  const Account({Key? key}) : super(key: key);

  @override
  State<Account> createState() => _Profile_PageState();
}

// ignore: camel_case_types
class _Profile_PageState extends State<Account> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
        title: Row(children: const <Widget>[
          Icon(
            Icons.person,
            color: Colors.black,
          ),
          Text("Edit Profile", style: TextStyle(color: Colors.black)),
        ], mainAxisAlignment: MainAxisAlignment.start),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: 200,
                    height: 200,
                    padding: EdgeInsets.all(3),
                    child: Image.asset(
                      "assets/user.jpg",
                    ))
              ],
            ),
            Divider(
              height: MediaQuery.of(context).size.width * 0.05,
              color: Colors.white,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: TextField(
                      decoration: InputDecoration(
                    labelText: "Enter New email",
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.all(20),
                  )),
                ),
              ],
            ),
            const Divider(
              height: 20,
              color: Colors.white,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Update"),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

//HOME SCREEN

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  State<HomeScreen> createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Shop",
        ),
        elevation: 0,
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    child: ImageSlideshow(
                        children: [
                          Image.asset('assets/sale.png'),
                          Image.asset('assets/sales banner.jpg')
                        ],
                        initialPage: 0,
                        autoPlayInterval: 3000,
                        indicatorColor: Colors.white,
                        isLoop: true),
                    color: Color.fromARGB(255, 52, 57, 61),
                  ),
                ),
              ],
            ),
            Divider(),
            Expanded(
              child: GridView.count(
                crossAxisCount: 2,
                children: List.generate(4, (index) {
                  return GridTile(
                    key: Key('0'),
                    header: Text("Sweatpant"),
                    child: Image.asset("assets/sweat.png"),
                  );
                }),
              ),
            )
          ]),
    );
  }
}

class Search_Screen extends StatefulWidget {
  const Search_Screen({Key? key}) : super(key: key);

  @override
  State<Search_Screen> createState() => _Search_ScreenState();
}

class _Search_ScreenState extends State<Search_Screen> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text("Search"),
      actions: [
        IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Searchbar()),
              );
            },
            icon: Icon(Icons.search))
      ],
    );
  }
}

class Searchbar extends StatefulWidget {
  const Searchbar({Key? key}) : super(key: key);

  @override
  State<Searchbar> createState() => _SearchbarState();
}

class _SearchbarState extends State<Searchbar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
      actions: [
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: const TextField(
            toolbarOptions: ToolbarOptions(
                copy: true, paste: true, selectAll: true, cut: true),
            decoration: InputDecoration(
                label: Text("Search"), icon: Icon(Icons.search)),
          ),
        )
      ],
    ));
  }
}

class WishList extends StatelessWidget {
  const WishList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const [
            Text(
              "Wish List",
            ),
          ],
        ),
        actions: [],
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: ListView(children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                width: 300,
                height: 400,
                child: Image.asset("assets/hoodie.jpg")),
            const Text(
              "ZAR 500",
            )
          ],
        ),
      ]),
    );
  }
}
